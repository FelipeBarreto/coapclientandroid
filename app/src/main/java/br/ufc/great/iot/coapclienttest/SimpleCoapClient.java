package br.ufc.great.iot.coapclienttest;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import org.ws4d.coap.Constants;
import org.ws4d.coap.connection.BasicCoapChannelManager;
import org.ws4d.coap.interfaces.CoapChannelManager;
import org.ws4d.coap.interfaces.CoapClient;
import org.ws4d.coap.interfaces.CoapClientChannel;
import org.ws4d.coap.interfaces.CoapRequest;
import org.ws4d.coap.interfaces.CoapResponse;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Felipe on 25/09/2016.
 */
public class SimpleCoapClient implements CoapClient{

    private static final String SERVER_ADDRESS = "192.168.25.69";
    private static final int PORT = Constants.COAP_DEFAULT_PORT;
    private static final String RESOURCE_URI = "/test/light"; // Put your resource path here

    private static SimpleCoapClient instance;

    private Activity context;

    CoapChannelManager channelManager = null;
    CoapClientChannel clientChannel = null;

    private SimpleCoapClient(){
        // Just making it private.
    }

    public static SimpleCoapClient getInstance(){
        if(instance == null){
            instance = new SimpleCoapClient();
        }
        return instance;
    }

    public void connect(Activity context){
        this.context = context;
        new Thread(new Runnable() {
            @Override
            public void run() {
                channelManager = BasicCoapChannelManager.getInstance();
                try {
                    clientChannel = channelManager.connect(SimpleCoapClient.this, InetAddress.getByName(SERVER_ADDRESS), PORT);

                    // The first operation made as soon as the connection is active
                    doFirstRequest();

                } catch(UnknownHostException ex){
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    private void doFirstRequest(){
        // Do your stuff here
        // E.g., Perform a GET request with observeOption enabled to start monitoring a resource
    }

    public void updateResource(float value){
        if(clientChannel == null){
            return;
        }

        // Update the resouce value on the server
        // You can do it by performing a PUT operation for that resource
    }

    @Override
    public void onResponse(CoapClientChannel channel, final CoapResponse response) {
        Log.d("CoAP Response", response + " // Payload = " + new String(response.getPayload()));

        // Handle the CoAP Response here
        // If you need to update the UI, remember to call it on the UI Thread
    }

    @Override
    public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, "Connection Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
